# how-to-build-myself

Open source for self management .

将自己的阅读、学习、实践、思考等记录。使用 markdown 编写，提交到 gitlab，使用 jenkins 自动更新 docker 镜像，后端 nginx 代理域名访问页面，页面由 markdown 自动转换为 html。

